﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridHelper 
{
    public readonly int rLength;//行长度 5
    public readonly int cLength;//列长度 8

    public GridHelper(Rc rc)
    {
        this.rLength = rc.r;
        this.cLength = rc.c;
    }

    public GridHelper(int rLength, int cLength)
    {
        this.rLength = rLength;
        this.cLength = cLength;
    }

    //把一维索引转换为2为索引的方法
    public Rc GetRc(int index) //8
    {
        int r = index / cLength;
        int c = index % cLength;
        return new Rc(r,c);
    }
    //把二维索引转换为1维索引
    public int GetIndex(Rc rc) //1,0
    {
        return cLength * rc.r + rc.c;
    }
    public int GetIndex(int r, int c) //1,0
    {
        Rc rc = new Rc(r, c);
        return GetIndex(rc);
    }

    public bool TryGet(Direction dir,int index,out int newIndex)
    {
        Rc rc = GetRc(index);//拿到二维索引

        bool outRange = false;

        switch (dir)
        {
            case Direction.up:
                outRange = rc.r - 1 < 0;
                rc.r -= 1;
                break;
            case Direction.down:
                outRange = rc.r+ 1 >= rLength;
                rc.r += 1;
                break;
            case Direction.left:
                outRange = rc.c - 1 < 0;
                rc.c -= 1;
                break;
            case Direction.right:
                outRange = rc.c + 1 >= cLength;
                rc.c += 1;
                break;
            case Direction.upLeft:
                outRange = rc.r - 1 < 0 || rc.c - 1 < 0;
                rc.r -= 1;
                rc.c -= 1;
                break;
            case Direction.upRight:
                outRange = rc.r - 1 < 0 || rc.c + 1 >= cLength;
                rc.r -= 1;
                rc.c += 1;
                break;
            case Direction.downLeft:
                outRange = rc.r + 1 >= rLength || rc.c - 1 < 0;
                rc.r += 1;
                rc.c -= 1;
                break;
            case Direction.downRight:
                outRange = rc.r + 1 >= rLength || rc.c + 1 >= cLength;
                rc.r += 1;
                rc.c += 1;
                break;
            default:
                break;
        }

        if (outRange)//判断上面还有没有
        {
            newIndex = index;
            return false;   
        }
        newIndex = GetIndex(rc);
        return true;
    }
}

public  enum Direction
{
    up,
    down,
    left,
    right,
    upLeft,
    upRight,
    downLeft,
    downRight,
}
[System.Serializable]
public  struct Rc //二维索引
{
    public int r;
    public int c;

    public Rc(int r, int c)
    {
        this.r = r;
        this.c = c;
    }
    public override string ToString()
    {
        return string.Format("{0},{1}", r, c);
    }

    #region 运算符重载
    public static Rc operator +(Rc a, Rc b) 
    {
        a.r += b.r;
        a.c += b.c;
        return a;
    }
    public static Rc operator -(Rc a, Rc b)
    {
        a.r -= b.r;
        a.c -= b.c;
        return a;
    }
    public static Rc operator *(Rc a, Rc b)
    {
        a.r *= b.r;
        a.c *= b.c;
        return a;
    }
    public static bool operator ==(Rc a, Rc b)
    {
        return  a.r == b.r&&  a.c == b.c   ;
    }
    public static bool operator !=(Rc a, Rc b)
    {
        return a.r != b.r || a.c != b.c;
    }

    #endregion

    #region 静态属性

    private static Rc up = new Rc(1,0);
    public static Rc Up { get { return up; } }

    private static Rc down = new Rc(-1, 0);
    public static Rc Down { get { return down; } }

    private static Rc right = new Rc(0, 1);
    public static Rc Right { get { return right; } }

    private static Rc left = new Rc(0, -1);
    public static Rc Left { get { return left; } }

    private static Rc one = new Rc(1,1 );
    public static Rc Nne { get { return one; } }

    private static Rc zero = new Rc(0, 0);
    public static Rc Zero { get { return zero; } }

    #endregion
}
