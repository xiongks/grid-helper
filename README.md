# GridHelper

#### 介绍
把一维当二维使用的工具类,只关心索引.不关心是数组还是List还是transform
附带 Unity扫雷游戏demo

## 如何使用 
#### 1创建助手对象
```csharp
 GridHelper gHelper = new GridHelper(3, 4);//表示3行四列
```
#### 2转换一维索引到二维索引
```csharp
Rc rc= gHelper.GetRc(4);//获取4对应的一维索引
print(rc);//输出 1,0

```

#### 3转换二维索引到一维索引
```csharp
int index= gHelper.GetIndex(new Rc(1,0));//获取4对应的一维索引
print(index);//输出 4
```

#### 4使用TryGet方法获取周围8个相邻的索引

```csharp
public bool TryGet(Direction dir,int index,out int newIndex)
//这个方法返回布尔值,为flase则表示超出界限
//参数描述
//   dir:方向
//   index:当前位置
//   newIndex:对应方向的相邻索引(如果方法返回false,则newIndex返回index)

```

  